var escena = (function () {

    var canvas = document.getElementById("art");
    canvas.width = document.body.clientWidth;
    canvas.height = document.body.clientHeight;
    var point = new obelisk.Point(500, 100);
    var pixelView = new obelisk.PixelView(canvas, point);

    function dibujarCasa() {
        dibujarSuelo();
        dibujarParedes();
        dibujarTecho();
    }

    function dibujarSuelo() {
        var brick;
        var point3D;
        var color;
        var dimension = new obelisk.BrickDimension(20, 20);
        color = new obelisk.SideColor(0x0FC616, 0x0FC616);
        brick = new obelisk.Brick(dimension, color);
        for (var x = 0; x < 20; x++) {
            for (var y = 0; y < 20; y++) {
                point3D = new obelisk.Point3D(x * 20, y * 20, 0);
                pixelView.renderObject(brick, point3D);
            }
        }
    }
    function dibujarParedes() {
        var dimension = new obelisk.CubeDimension(60, 120, 50);
        var color = new obelisk.CubeColor().getByHorizontalColor(0xAF4C00);
        var cube = new obelisk.Cube(dimension, color, false);
        point3D = new obelisk.Point3D(100, 100, 0);
        pixelView.renderObject(cube, point3D);
    }

    function dibujarTecho() {
        var dimension = new obelisk.PyramidDimension(60, false);
        var color = new obelisk.PyramidColor().getByRightColor(obelisk.ColorPattern.YELLOW);
        var pyramid = new obelisk.Pyramid(dimension, color);
        point3D = new obelisk.Point3D(100, 100, 50);
        pixelView.renderObject(pyramid, point3D);
        point3D = new obelisk.Point3D(100, 160, 50);
        pixelView.renderObject(pyramid, point3D);
    }

    return {
        dibujarCasa: dibujarCasa
    };
})();

escena.dibujarCasa();