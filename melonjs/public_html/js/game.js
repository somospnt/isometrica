var game = {
    width: function() {
        var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth || 0;
        return width;
    },
    height: function() {
        var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight || 0;
        return height - 5;
    },
    onload: function() {
        me.video.init("screen", me.video.CANVAS, game.width(), game.height());
        me.loader.onload = game.loaded.bind(this);
        me.audio.init("mp3,ogg");
        me.loader.preload(game.resources);
        me.state.change(me.state.LOADING);
    },
    loaded: function() {
        me.state.set(me.state.PLAY, new game.PlayScreen());


       
        me.pool.register("jugador", game.PlayerEntity);
        me.pool.register("moneda", game.CoinEntity);
        me.pool.register("enemigo", game.Enemigo);

        me.input.bindKey(me.input.KEY.LEFT, "mapa-izquierda");
        me.input.bindKey(me.input.KEY.RIGHT, "mapa-derecha");
        me.input.bindKey(me.input.KEY.UP, "mapa-arriba");
        me.input.bindKey(me.input.KEY.DOWN, "mapa-abajo");
        me.input.bindKey(me.input.KEY.ENTER, "mapa-shake");

        me.state.change(me.state.PLAY);
    }
};

