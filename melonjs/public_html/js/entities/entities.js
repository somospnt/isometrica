game.PlayerEntity = me.Entity.extend({
    init: function(x, y, settings) {
        this._super(me.Entity, 'init', [x, y, settings]);
        this.body.setVelocity(2, 2);
        this.body.gravity = 0;
        me.game.viewport.follow(this.pos, me.game.viewport.AXIS.BOTH);
        this.alwaysUpdate = true;

        this.renderable = new me.AnimationSheet(0, 0, {
            image: me.loader.getImage('player-animationsheet'),
            spritewidth: 32,
            spriteheight: 32
        });
        this.renderable.addAnimation("idle", [4]);
        this.renderable.addAnimation("caminar-abajo", [0, 1, 2]);
        this.renderable.addAnimation("caminar-izquierda", [12, 13, 14]);
        this.renderable.addAnimation("caminar-derecha", [24, 25, 26]);
        this.renderable.addAnimation("caminar-arriba", [36, 37, 38]);
        this.renderable.addAnimation("caminar-izquierda-arriba", [15, 16, 17]);
        this.renderable.addAnimation("caminar-izquierda-abajo", [3, 4, 5]);
        this.renderable.addAnimation("caminar-derecha-arriba", [39, 40, 41]);
        this.renderable.addAnimation("caminar-derecha-abajo", [27, 28, 29]);
        this.renderable.setCurrentAnimation("idle");

        me.input.bindKey(me.input.KEY.A, "jugador-izquierda");
        me.input.bindKey(me.input.KEY.D, "jugador-derecha");
        me.input.bindKey(me.input.KEY.W, "jugador-arriba");
        me.input.bindKey(me.input.KEY.S, "jugador-abajo");

    },
    update: function(dt) {

        if (game.utils.areKeyPressed("jugador-izquierda", "jugador-arriba")) {
            this.body.vel.x -= this.body.accel.x * me.timer.tick;
            this.body.vel.y -= this.body.accel.y * me.timer.tick;
            game.utils.setAnimationIfNotCurrent.bind(this)("caminar-izquierda-arriba");
        }
        else if (game.utils.areKeyPressed("jugador-izquierda", "jugador-abajo")) {
            this.body.vel.x -= this.body.accel.x * me.timer.tick;
            this.body.vel.y += this.body.accel.y * me.timer.tick;
            game.utils.setAnimationIfNotCurrent.bind(this)("caminar-izquierda-abajo");
        }
        else if (game.utils.areKeyPressed("jugador-derecha", "jugador-arriba")) {
            this.body.vel.x += this.body.accel.x * me.timer.tick;
            this.body.vel.y -= this.body.accel.y * me.timer.tick;
            game.utils.setAnimationIfNotCurrent.bind(this)("caminar-derecha-arriba");
        }
        else if (game.utils.areKeyPressed("jugador-derecha", "jugador-abajo")) {
            this.body.vel.x += this.body.accel.x * me.timer.tick;
            this.body.vel.y += this.body.accel.y * me.timer.tick;
            game.utils.setAnimationIfNotCurrent.bind(this)("caminar-derecha-abajo");
        }
        else if (game.utils.areKeyPressed("jugador-izquierda")) {
            this.body.vel.x -= this.body.accel.x * me.timer.tick;
            this.body.vel.y = 0;
            game.utils.setAnimationIfNotCurrent.bind(this)("caminar-izquierda");
        }
        else if (game.utils.areKeyPressed("jugador-derecha")) {
            this.body.vel.x += this.body.accel.x * me.timer.tick;
            this.body.vel.y = 0;
            game.utils.setAnimationIfNotCurrent.bind(this)("caminar-derecha");
        }
        else if (game.utils.areKeyPressed("jugador-arriba")) {
            this.body.vel.x = 0;
            this.body.vel.y -= this.body.accel.x * me.timer.tick;
            game.utils.setAnimationIfNotCurrent.bind(this)("caminar-arriba");
        }
        else if (game.utils.areKeyPressed("jugador-abajo")) {
            this.body.vel.x = 0;
            this.body.vel.y += this.body.accel.x * me.timer.tick;
            game.utils.setAnimationIfNotCurrent.bind(this)("caminar-abajo");
        }

        if (game.utils.noKeyPressed("jugador-izquierda", "jugador-derecha", "jugador-arriba", "jugador-abajo")) {
            this.renderable.setCurrentAnimation("idle");
            this.body.vel.x = 0;
            this.body.vel.y = 0;
        }

        this.body.update(dt);

        me.collision.check(this, true, this.collideHandler.bind(this), true);

        // update animation if necessary
        if (this.body.vel.x !== 0 || this.body.vel.y !== 0) {
            this._super(me.Entity, 'update', [dt]);
            return true;
        }

        // else inform the engine we did not perform any update (e.g. position, animation)
        return false;

    },
    collideHandler: function(response) {
        if (response.b.body.collisionType === me.collision.types.ENEMY_OBJECT) {
            this.renderable.flicker(750);
        }
    }
});


game.CoinEntity = me.CollectableEntity.extend({
    init: function(x, y, settings) {
        this._super(me.CollectableEntity, 'init', [x, y, settings]);
        this.body.onCollision = this.onCollision.bind(this);
    },
    onCollision: function() {
        me.audio.play("cling");
        this.body.setCollisionMask(me.collision.types.NO_OBJECT);
        me.game.world.removeChild(this);
    }
});

game.Enemigo = me.Entity.extend({
    init: function(x, y, settings) {
        this._super(me.Entity, 'init', [x, y, settings]);
        this.body.setVelocity(2, 2);
        this.body.gravity = 0;
        this.alwaysUpdate = false;
        this.renderable = new me.AnimationSheet(0, 0, {
            image: me.loader.getImage('enemy-animationsheet'),
            spritewidth: 96,
            spriteheight: 48
        });
        this.renderable.addAnimation("caminar-abajo", [0, 1, 2]);
        this.renderable.addAnimation("caminar-izquierda", [3, 4, 5]);
        this.renderable.addAnimation("caminar-derecha", [6, 7, 8]);
        this.renderable.addAnimation("caminar-arriba", [9, 10, 11]);

        this.startY = this.pos.y;
        this.startX = this.pos.x;
        this.endY = this.pos.y + 120;
        this.endX = this.pos.x + 120;

        this.rumbo = "abajo";
        this.renderable.setCurrentAnimation("caminar-abajo");
    },
    update: function(dt) {
        if (this.rumbo === "abajo" && this.pos.y < this.endY) {
            this.body.vel.y += 10;
            this.body.update(dt);
            return true;
        } else if (this.rumbo === "abajo") {
            this.body.vel.y = 0;
            this.rumbo = "derecha";
            this.renderable.setCurrentAnimation("caminar-derecha");
        }

        if (this.rumbo === "derecha" && this.pos.x < this.endX) {
            this.body.vel.x += 10;
            this.body.update(dt);
            return true;
        } else if (this.rumbo === "derecha") {
            this.body.vel.x = 0;
            this.rumbo = "arriba";
            this.renderable.setCurrentAnimation("caminar-arriba");
        }

        if (this.rumbo === "arriba" && this.pos.y > this.startY) {
            this.body.vel.y -= 10;
            this.body.update(dt);
            return true;
        } else if (this.rumbo === "arriba") {
            this.body.vel.y = 0;
            this.rumbo = "izquierda";
            this.renderable.setCurrentAnimation("caminar-izquierda");
        }

        if (this.rumbo === "izquierda" && this.pos.x > this.startX) {
            this.body.vel.x -= 10;
            this.body.update(dt);
            return true;
        } else if (this.rumbo === "izquierda") {
            this.body.vel.x = 0;
            this.rumbo = "abajo";
            this.renderable.setCurrentAnimation("caminar-abajo");
        }

        return false;
    }
});