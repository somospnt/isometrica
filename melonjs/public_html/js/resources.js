game.resources = [
    {name: "mapaAfuera", type: "tmx", src: "data/map/terreno.tmx"},
    {name: "mapaCueva", type: "tmx", src: "data/map/cueva.tmx"},
    {name: "terrain_0", type: "image", src: "data/map/terrain/png/terrain_0.png"},
    {name: "player-animationsheet", type: "image", src: "data/objects/player-animationsheet.png"},
    {name: "enemy-animationsheet", type: "image", src: "data/objects/enemy.png"},
    {name: "coin-animationsheet", type: "image", src: "data/objects/coin.png"},
    {name: "cling", type: "audio", src: "data/sfx/"}
];
