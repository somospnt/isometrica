game.utils = {
    /** Return true if all keys are pressed.
     * @param varargs, each argument is a key to test.
     */
    areKeyPressed: function () {
        for (var i = 0; i < arguments.length; i++) {
            if (!me.input.isKeyPressed(arguments[i])) {
                return false;
            }
        }
        return true;
    },

    /** Sets the animation for the caller, if it is not already setted. */
    setAnimationIfNotCurrent: function (name) {
        if (!this.renderable.isCurrentAnimation(name)) {
            this.renderable.setCurrentAnimation(name);
        }
    },

    /**
     * Return true if no key is pressed.
     * @param varargs, each argument is a key to test.
     */
    noKeyPressed: function () {
        for (var i = 0; i < arguments.length; i++) {
            if (me.input.isKeyPressed(arguments[i])) {
                return false;
            }
        }
        return true;
    }
};
