game.PlayScreen = me.ScreenObject.extend({
    /** action to perform on state change */
    onResetEvent: function () {
        me.levelDirector.loadLevel("mapaAfuera");
        this.handle = me.event.subscribe(me.event.KEYDOWN, this.keyPressed.bind(this));
        //centrar viewport
        me.game.viewport.move(me.game.currentLevel.width / 2 - (game.width() / 2), me.game.currentLevel.height / 2 - (game.height() / 2));
    },
    keyPressed: function (action, keyCode, edge) {
        switch (action) {
            case "mapa-izquierda":
                me.game.viewport.move(-(me.game.currentLevel.tilewidth / 1), 0);
                break;
            case "mapa-derecha":
                me.game.viewport.move(me.game.currentLevel.tilewidth / 1, 0);
                break;
            case "mapa-arriba":
                me.game.viewport.move(0, -(me.game.currentLevel.tileheight / 1));
                break;
            case "mapa-abajo":
                me.game.viewport.move(0, me.game.currentLevel.tileheight / 1);
                break;
            case "mapa-shake":
                me.game.viewport.shake(30, 750);
                break;
        }

        me.game.repaint();
    }
});
