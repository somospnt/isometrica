<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Road Tiles" tilewidth="101" tileheight="80">
 <tile id="0">
  <image width="100" height="63" source="png/bridgeEast.png"/>
 </tile>
 <tile id="1">
  <image width="100" height="63" source="png/bridgeNorth.png"/>
 </tile>
 <tile id="2">
  <image width="100" height="65" source="png/crossroad.png"/>
 </tile>
 <tile id="3">
  <image width="100" height="58" source="png/dirt.png"/>
 </tile>
 <tile id="4">
  <image width="100" height="66" source="png/dirtHigh.png"/>
 </tile>
 <tile id="5">
  <image width="100" height="65" source="png/grass.png"/>
 </tile>
 <tile id="6">
  <image width="100" height="80" source="png/hillCornerEast.png"/>
 </tile>
 <tile id="7">
  <image width="100" height="65" source="png/hillCornerNW.png"/>
 </tile>
 <tile id="8">
  <image width="100" height="65" source="png/hillCornerSE.png"/>
 </tile>
 <tile id="9">
  <image width="100" height="65" source="png/hillCornerWest.png"/>
 </tile>
 <tile id="10">
  <image width="100" height="80" source="png/hillEast.png"/>
 </tile>
 <tile id="11">
  <image width="100" height="80" source="png/hillNorth.png"/>
 </tile>
 <tile id="12">
  <image width="100" height="80" source="png/hillRoadEast.png"/>
 </tile>
 <tile id="13">
  <image width="100" height="80" source="png/hillRoadNorth.png"/>
 </tile>
 <tile id="14">
  <image width="101" height="65" source="png/hillRoadSouth.png"/>
 </tile>
 <tile id="15">
  <image width="101" height="65" source="png/hillRoadWest.png"/>
 </tile>
 <tile id="16">
  <image width="100" height="65" source="png/hillSouth.png"/>
 </tile>
 <tile id="17">
  <image width="100" height="65" source="png/hillWest.png"/>
 </tile>
 <tile id="18">
  <image width="100" height="66" source="png/lot.png"/>
 </tile>
 <tile id="19">
  <image width="100" height="65" source="png/lotCornerEast.png"/>
 </tile>
 <tile id="20">
  <image width="100" height="66" source="png/lotCornerNorth.png"/>
 </tile>
 <tile id="21">
  <image width="100" height="66" source="png/lotCornerSouth.png"/>
 </tile>
 <tile id="22">
  <image width="100" height="66" source="png/lotCornerWest.png"/>
 </tile>
 <tile id="23">
  <image width="100" height="65" source="png/lotEast.png"/>
 </tile>
 <tile id="24">
  <image width="100" height="66" source="png/lotExitEast.png"/>
 </tile>
 <tile id="25">
  <image width="100" height="66" source="png/lotExitNorth.png"/>
 </tile>
 <tile id="26">
  <image width="100" height="66" source="png/lotExitSouth.png"/>
 </tile>
 <tile id="27">
  <image width="100" height="66" source="png/lotExitWest.png"/>
 </tile>
 <tile id="28">
  <image width="100" height="65" source="png/lotNorth.png"/>
 </tile>
 <tile id="29">
  <image width="100" height="65" source="png/lotPark.png"/>
 </tile>
 <tile id="30">
  <image width="100" height="65" source="png/lotSouth.png"/>
 </tile>
 <tile id="31">
  <image width="100" height="65" source="png/lotWest.png"/>
 </tile>
 <tile id="32">
  <image width="100" height="65" source="png/roadCornerES.png"/>
 </tile>
 <tile id="33">
  <image width="100" height="65" source="png/roadCornerNE.png"/>
 </tile>
 <tile id="34">
  <image width="100" height="65" source="png/roadCornerNW.png"/>
 </tile>
 <tile id="35">
  <image width="100" height="65" source="png/roadCornerWS.png"/>
 </tile>
 <tile id="36">
  <image width="100" height="65" source="png/roadEast.png"/>
 </tile>
 <tile id="37">
  <image width="100" height="65" source="png/roadEndEast.png"/>
 </tile>
 <tile id="38">
  <image width="100" height="65" source="png/roadEndNorth.png"/>
 </tile>
 <tile id="39">
  <image width="100" height="65" source="png/roadEndSouth.png"/>
 </tile>
 <tile id="40">
  <image width="100" height="65" source="png/roadEndWest.png"/>
 </tile>
 <tile id="41">
  <image width="100" height="65" source="png/roadNorth.png"/>
 </tile>
 <tile id="42">
  <image width="100" height="65" source="png/roadTEast.png"/>
 </tile>
 <tile id="43">
  <image width="100" height="65" source="png/roadTNorth.png"/>
 </tile>
 <tile id="44">
  <image width="100" height="65" source="png/roadTSouth.png"/>
 </tile>
 <tile id="45">
  <image width="100" height="65" source="png/roadTWest.png"/>
 </tile>
 <tile id="46">
  <image width="14" height="36" source="png/treeConiferShort.png"/>
 </tile>
 <tile id="47">
  <image width="19" height="48" source="png/treeConiferTall.png"/>
 </tile>
 <tile id="48">
  <image width="11" height="33" source="png/treeShort.png"/>
 </tile>
 <tile id="49">
  <image width="11" height="33" source="png/treeShort_autumn.png"/>
 </tile>
 <tile id="50">
  <image width="11" height="44" source="png/treeTall.png"/>
 </tile>
 <tile id="51">
  <image width="11" height="44" source="png/treeTall_autumn.png"/>
 </tile>
 <tile id="52">
  <image width="100" height="64" source="png/water.png"/>
 </tile>
 <tile id="53">
  <image width="100" height="65" source="png/waterBeachCornerEast.png"/>
 </tile>
 <tile id="54">
  <image width="100" height="63" source="png/waterBeachCornerNorth.png"/>
 </tile>
 <tile id="55">
  <image width="100" height="66" source="png/waterBeachCornerSouth.png"/>
 </tile>
 <tile id="56">
  <image width="100" height="65" source="png/waterBeachCornerWest.png"/>
 </tile>
 <tile id="57">
  <image width="100" height="64" source="png/waterBeachEast.png"/>
 </tile>
 <tile id="58">
  <image width="100" height="64" source="png/waterBeachNorth.png"/>
 </tile>
 <tile id="59">
  <image width="100" height="66" source="png/waterBeachSouth.png"/>
 </tile>
 <tile id="60">
  <image width="100" height="66" source="png/waterBeachWest.png"/>
 </tile>
 <tile id="61">
  <image width="100" height="65" source="png/waterCornerEast.png"/>
 </tile>
 <tile id="62">
  <image width="100" height="64" source="png/waterCornerNorth.png"/>
 </tile>
 <tile id="63">
  <image width="100" height="66" source="png/waterCornerSouth.png"/>
 </tile>
 <tile id="64">
  <image width="100" height="65" source="png/waterCornerWest.png"/>
 </tile>
 <tile id="65">
  <image width="100" height="63" source="png/waterEast.png"/>
 </tile>
 <tile id="66">
  <image width="100" height="63" source="png/waterNorth.png"/>
 </tile>
 <tile id="67">
  <image width="100" height="65" source="png/waterSouth.png"/>
 </tile>
 <tile id="68">
  <image width="100" height="65" source="png/waterWest.png"/>
 </tile>
</tileset>
