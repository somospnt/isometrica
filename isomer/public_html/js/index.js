var ejemplos = (function () {
    var canvas = document.getElementById("art");
    canvas.width = document.body.clientWidth;
    canvas.height = 760;

    var Shape = Isomer.Shape;
    var Point = Isomer.Point;
    var Color = Isomer.Color;

    var iso = new Isomer(document.getElementById("art"), {
        scale: 40
    });


    function dibujarCilindro() {
        var cilindro = Shape.Cylinder(Point(4,4,1), 0.5, 20, 5);
        iso.add(cilindro, new Color(200, 0, 0));
    }


    function dibujarPisoAtrasAdelante() {
        var piso;
        var color;
        var verdePasto1 = new Color(65, 131, 19);
        var verdePasto2 = new Color(70, 125, 30);
        for (var x = 20; x > 0; x--) {
            for (var y = 20; y > 0; y--) {
                piso = Shape.Prism(Point(x, y, 0), 1, 1, 1);

                if (Math.random() < .5) {
                    color = verdePasto1;
                }
                else {
                    color = verdePasto2;
                }
                iso.add(piso, color);
            }
        }
    }

    function dibujarPisoAdelanteAtras() {
        var piso;
        var color;
        var verdePasto1 = new Color(65, 131, 19);
        var verdePasto2 = new Color(70, 125, 30);
        for (var x = 0; x < 20; x++) {
            for (var y = 0; y < 20; y++) {
                piso = Shape.Prism(Point(x, y, 0), 1, 1, 1);

                if (Math.random() < .5) {
                    color = verdePasto1;
                }
                else {
                    color = verdePasto2;
                }
                iso.add(piso, color);
            }
        }
    }

    return {
        dibujarPisoAtrasAdelante: dibujarPisoAtrasAdelante,
        dibujarPisoAdelanteAtras: dibujarPisoAdelanteAtras,
        dibujarCilindro: dibujarCilindro
    };
})();

//ejemplos.dibujarPisoAdelanteAtras();
ejemplos.dibujarPisoAtrasAdelante();
ejemplos.dibujarCilindro();

